# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/26 18:45:59 by dtitenko          #+#    #+#              #
#    Updated: 2017/04/09 23:13:33 by dtitenko         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf
LIBNAME = libft
LIBDIR = ./lib
LIBINCLUDEFOLDERS = ./lib/includes
INCLUDES = ./

SOURCES_FOLDER = src/
OBJECTS_FOLDER = obj/

SOURCES = \
		ft_printf.c \
		ft_xprintf.c \
		pars_format.c \
		ft_putfld.c \
		ft_tobs.c \
		ft_c_s_lc_ls_di.c \
		ft_n_p_boux_ldlolu.c \
		ft_f.c \
		ft_undefspec.c \
		helpers.c \
		ft_colors.c\
		ft_output.c \
		ft_ftoa.c \
		ft_ftoa_s.c \
		ft_itoa.c \
		ft_ltoa_base.c \
		ft_tolower.c \
		ft_toupper.c \
		ft_ulltoa_base.c \
		ft_wcstombs.c \
		ft_wctomb.c \
		ft_bzero.c \
		ft_memalloc.c \
		ft_memset.c \
		ft_memcpy.c \
		ft_isdigit.c \
		ft_strcat.c \
		ft_strchr.c \
		ft_strcpy.c \
		ft_strdel.c \
		ft_strdup.c \
		ft_strjoin.c \
		ft_strlen.c \
		ft_strncat.c \
		ft_strncmp.c \
		ft_strncpy.c \
		ft_strnequ.c \
		ft_strnew.c \
		ft_abs.c \
		ft_pow.c \
		ft_powf.c \
		ft_round.c

OBJECTS = $(SOURCES:.c=.o)
OBJECTS := $(subst /,__,$(OBJECTS))
OBJECTS := $(addprefix $(OBJECTS_FOLDER), $(OBJECTS))
SOURCES := $(addprefix $(SOURCES_FOLDER),$(SOURCES))

CC = gcc
AR = ar
CFLAGS = -Wall -Werror -Wextra


# Colors

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m


# Basic Rules

.PHONY: all re clean fclean tests

all: $(NAME)

tests: all
	$(CC) -I$(INCLUDES) ./tests/basic_tests.c -L. -lftprintf -o ./basic_test

$(OBJECTS_FOLDER)%.o: $(SOURCES_FOLDER)%.c
	@mkdir -p $(OBJECTS_FOLDER)
	$(CC) -c $(CFLAGS) -I$(INCLUDES) -o $@ $<

$(NAME): $(OBJECTS)
#	@printf "$(SILENT_COLOR)Compiling Libftprintf...$(NO_COLOR)\n"
	@$(AR) -rcs $(NAME).a $(OBJECTS)
#	@printf "$(OK_COLOR)Successful ✓$(NO_COLOR)\n"

clean:
	rm -f $(OBJECTS)
	rm -rf $(OBJECTS_FOLDER)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Objects libftprintf$(NO_COLOR)\n"

fclean: clean
	rm -f ./basic_test
	rm -f $(NAME).a
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Library libftprintf$(NO_COLOR)\n"

re: fclean all