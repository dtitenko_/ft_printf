/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_f.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 02:58:17 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:07 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include "includes/ft_strings.h"

void	ft_fe(t_format *px, va_list pap, char code, char *ac)
{
	if (ft_strchr("fFeE", code))
	{
		(*px).v.ld = ((*px).qual == 'L') ? va_arg(pap, long double) :
					va_arg(pap, double);
		if ((*px).v.ld < 0)
			ac[(*px).n0++] = '-';
		else if ((*px).flags & FPL)
			ac[(*px).n0++] = '+';
		else if ((*px).flags & FSP)
			ac[(*px).n0++] = ' ';
		ft_ldtob(px, code);
	}
}
