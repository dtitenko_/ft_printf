/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_colors.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 19:18:30 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:13:59 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_strings.h"
#include "includes/ft_convert.h"
#include "includes/ft_printf.h"

static int g_colors[] = {BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE};
static char g_colorsc[] = "frgybmcw";

#define NO_COLOR "\033[0m"

int		ft_get_color(const char **s, va_list pap)
{
	char		*t;
	int			color;

	color = -2;
	if ((t = ft_strchr(g_colorsc, **s)) && (*s)++)
		color = g_colors[t - g_colorsc];
	else if (ft_isdigit(**s))
	{
		color = 0;
		while (ft_isdigit(**s))
			color = color * 10 + *(*s)++ - '0';
	}
	else if (ft_strlen(*s) > 3 && ft_strnequ("eoc", *s, 3))
	{
		if (*((*s) += 3) == '}')
			color = -1;
	}
	else if (ft_strlen(*s) > 1 && **s == '*' && *((*s) += 1) == '}')
		color = (unsigned char)va_arg(pap, int);
	return (color);
}

void	ft_parse_color(const char **s, t_format *px, va_list pap)
{
	int			color;
	const char	*tmp;

	(*px).colorf = 0;
	(*px).width = 0;
	tmp = (*s)++;
	if (ft_strchr("fb", **s) && *(*s + 1) != '}')
		((*(*s)++) == 'b') ? ((*px).colorf |= BG) : ((*px).colorf |= FG);
	color = ft_get_color(s, pap);
	(*px).color = color;
	if ((**s) != '}' || color == -2)
	{
		(*s) = tmp;
		(*px).colorf |= NPCOLOR;
	}
	else
		(*px).colorf |= PCOLOR;
}

void	ft_color(t_format *px)
{
	char	*color;

	if ((*px).colorf & PCOLOR)
	{
		(*px).s = ft_strnew(12);
		if ((*px).color == -1)
			ft_strcat((*px).s, NO_COLOR);
		else
		{
			color = ft_itoa((unsigned char)(*px).color);
			ft_strcat((*px).s, "\033[");
			((*px).colorf & BG) ? ft_strcat((*px).s, "48;5;")
								: ft_strcat((*px).s, "38;5;");
			ft_strcat((*px).s, color);
			ft_strcat((*px).s, "m");
			ft_strdel(&color);
		}
		(*px).n1 = (int)ft_strlen((*px).s);
	}
}
