#MODIFIERS
| Modifier | `d`, `i`      | `o`, `u`, `x`, `X`   | `n`           |
|----------|---------------|----------------------|---------------|
| `hh`     | `signed char` | `unsigned char`      |`signed char *`|
| `h`      | `short`       | `unsigned short`     | `short *`     |
| `l`      | `long`        | `unsigned long`      | `long *`      |
| `ll`     | `long long`   | `unsigned long long` | `long long *` |
  
| Modifier | `a`, `A`, `e`, `E`, `f`, `F`, `g`, `G`          |
|----------|-------------------------------------------------|
| `l`      | `double` (ignored, same behavior as without it) |
| `L`      | `long double`|
  
| Modifier | `c`      | `s`         |
|----------|----------|-------------|
| `l`      | `wint_t` | `wchar_t *` |

#Conversion specifiers
| Conversion specifier | Meaning |
|----------------------|---------|
| `diouxX` | The `int` (or appropriate variant) argument is converted to signed decimal (d and i), unsigned octal (o), unsigned decimal (u), or unsigned hexadecimal (x and X) notation.  The letters ''abcdef'' are used for x conversions; the letters ``ABCDEF'' are used for X conversions.  The precision, if any, gives the minimum number of digits that must appear; if the converted value requires fewer digits, it is padded on the left with zeros.|
| `DOU`    | The long int argument is converted to signed decimal, unsigned octal, or unsigned decimal, as if the format had been ld, lo, or lu respectively.  These conversion characters are deprecated, and will eventually disappear.|
| `eE`     | The double argument is rounded and converted in the style [-]d.ddde+-dd where there is one digit before the decimal-point character and the number of digits after it is equal to the precision; if the precision is missing, it is taken as 6; if the precision is zero, no decimal-point character|
| `p`      | The `void *` pointer argument is printed in hexadecimal (as if by `%#x` or `%#lx`).|